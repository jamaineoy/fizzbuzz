import pytest
# Tests for fizz
import fizz_buzz as fb

def test_assert_fizz_with_3():
    assert fb.Fizz(3) is True

def test_assert_fizz_with_2():
    assert fb.Fizz(2) is False

def test_assert_fizz_outcome_str():
     assert type(fb.FizzBuzzer(2)) is int
     assert fb.FizzBuzzer(3) == 'fizz'
     assert type(fb.FizzBuzzer(3)) is str

## Tests for buzz
def test_assert_buzz_with_5():
    assert fb.Buzz(5) is True

def test_assert_buzz_with_2():
    assert fb.Buzz(2) is False

def test_assert_buzz_outcome_str():
    assert type(fb.FizzBuzzer(2)) is int
    assert fb.FizzBuzzer(5) == 'buzz'
    assert type(fb.FizzBuzzer(5)) is str

# Tests for fizzbuzz
def test_assert_fizzbuzz_with_15():
    assert fb.FizzBuzz(15) is True

def test_assert_fizzbuzz_with_7():
    assert fb.FizzBuzz(7) is False

def test_assert_fizzbuzz_outcome_str():
    assert type(fb.FizzBuzzer(7)) is int
    assert fb.FizzBuzzer(15) == 'fizzbuzz'
    assert type(fb.FizzBuzzer(15)) is str

def test_assert_runfizzbuzz_with_num():
    assert fb.test_runFizzBuzz(10) == [1, 2, 'fizz', 4, 'buzz', 'fizz', 7, 8, 'fizz', 'buzz']
    assert fb.test_runFizzBuzz(5) == [1, 2, 'fizz', 4, 'buzz']
    assert fb.test_runFizzBuzz(15) == [1, 2, 'fizz', 4, 'buzz', 'fizz', 7, 8, 'fizz', 'buzz', 11, 'fizz', 13, 14, 'fizzbuzz']

def test_assert_runFB_with_num():
    assert fb.test_runFB(10) == [1, 2, 'fizz', 4, 'buzz', 'fizz', 7, 8, 'fizz', 'buzz']
    assert fb.test_runFB(5) == [1, 2, 'fizz', 4, 'buzz']
    assert fb.test_runFB(15) == [1, 2, 'fizz', 4, 'buzz', 'fizz', 7, 8, 'fizz', 'buzz', 11, 'fizz', 13, 14,'fizzbuzz']