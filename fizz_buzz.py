def Fizz(number):
    "Checks for multiples of 3"
    if number % 3 == 0:
        return True
    else:
        return False

def Buzz(number):
    "Checks for multiples of 5"
    if number % 5 == 0:
        return True
    else:
        return False

def FizzBuzz(number):
    "Checks for multiples of 3 and 5"
    if number % 3 == 0 and number % 5 == 0:
        return True
    else:
        return False

def FizzBuzzer(number):
    "Checks for multiples of 3 and 5"
    f = Fizz(number)
    b = Buzz(number)
    FB = FizzBuzz(number)

    if FB is True:
        return 'fizzbuzz'
    else:
        if f is True:
            return 'fizz'
        elif b is True:
            return 'buzz'
        else:
            return number

def runFizzBuzz():
    fizzbuzz_list = []
    number = prompt()
    for i in range(1, number+1):
        f = Fizz(i)
        b = Buzz(i)
        fb = FizzBuzz(i)
        if fb is True:
            fizzbuzz_list.append('fizzbuzz')
        else:
            if f is True:
                fizzbuzz_list.append('fizz')
            elif b is True:
                fizzbuzz_list.append('buzz')
            else:
                fizzbuzz_list.append(i)
    return fizzbuzz_list

def test_runFizzBuzz(number):
    fizzbuzz_list = []
    for i in range(1, number+1):
        f = Fizz(i)
        b = Buzz(i)
        fb = FizzBuzz(i)
        if fb is True:
            fizzbuzz_list.append('fizzbuzz')
        else:
            if f is True:
                fizzbuzz_list.append('fizz')
            elif b is True:
                fizzbuzz_list.append('buzz')
            else:
                fizzbuzz_list.append(i)
    return fizzbuzz_list

def FB(i):
    if i % 3 == 0 and i % 5 != 0:
        return 'fizz'
    elif i % 3 != 0 and i % 5 == 0:
        return 'buzz'
    elif i % 3 == 0 and i % 5 == 0:
        return 'fizzbuzz'
    else:
        return i

def test_runFB(number):
    fizzbuzz_list = []
    for i in range(1, number + 1):
        fizzbuzz_list.append(FB(i))
    return fizzbuzz_list

def runFB():
    fizzbuzz_list = []
    number = prompt()
    for i in range(1, number + 1):
        fizzbuzz_list.append(FB(i))
    return fizzbuzz_list


def prompt():
    "Function to display prompt, check if an integer has been entered and parses the value"
    try:
        A= input('Please enter a number: ')
        A = int(A)
    except ValueError:
        print("\n-----Invalid entry! Please enter a number!-----\n")
        prompt()
    else:
        if A < 0:
            prompt()
        else:
            return A


if __name__=='__main__':
    print(runFizzBuzz())