# Testing, System Testing and TDD
this repo is going to be a TDD exercise for fizzbuzz, while we discuss testing and system testing.


## Types of tests:
- Black box Testing:
  - Don't know how the code is running & don't care
  - Test the system as a whole
- White box Testing:
    - Know the ins and outs of code
    - Test individual sections
    - More like unit testing

## Levels of testing
- Unit testing (single functions and classes/methods)
- Integration testing (groups of functions and classes/methods together)
- System testing (Functions and classes working on a system (a server in port 80))

<img src="https://www.guru99.com/images/6-2015/052715_0904_GuidetoSDLC3.png">
![V Diagram](https://www.guru99.com/images/6-2015/052715_0904_GuidetoSDLC3.png)

## Importance of testing for DevOps and Agile
**In terms of agile, it ensures working code**

**In terms of DevOps, you are automating code to be deployed, therefore tests will ensure your team is automating good working code**

We all make mistakes, some can be more costly than others. If they end up in production they are usually even more expensive.

---

## FIZZ BUZZ!!
if number can be divided by 3: print FIZZ <br>
if number can be divided by 5: print BUZZ <br>
if number can be divided by both 3 and 5: print FIZZBUZZ

### Main user stories and Epics
[Epic] As a user I want a program that runs Fizzbuzz, up to any number should output a list and should be tested, so that I know it works.

[User story] As a user when the program gets a multiple of 3, its should print out fizz.<br>
[User story] As a user when the program gets a multiple of 5, its should print out buzz.<br>
[User story] As a user when the program gets a multiple of 3 and 5, its should print out fizzbuzz.<br>
[User story] As a user when I give program a number, it should play fizzbuzz up to said number.

---
### _**Trello:** https://trello.com/b/g7FtfPFJ/cohort7-fizzbuzz-tdd_